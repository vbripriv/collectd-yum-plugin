# Created by pyp2rpm-3.3.2
%global pypi_name collectd_yum

Name:           python-%{pypi_name}
Version:        1.4.0
Release:        1%{?dist}
Summary:        Collectd Plugin to retrieve the yum status

License:        Apache II
URL:            https://gitlab.cern.ch/monitoring/collectd-yum-plugin
Source0:        https://gitlab.cern.ch/monitoring/collectd-yum-plugin/-/archive/%{version}/collectd-yum-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

%description

%package -n python2-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{pypi_name}}

%description -n python2-%{pypi_name}

%prep
%autosetup -n collectd-yum-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post -n python2-%{pypi_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n python2-%{pypi_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files -n python2-%{pypi_name}
%doc README.rst NEWS.txt LICENSE
%{python_sitelib}/%{pypi_name}
%{python_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Mon Jan 27 2020 Diogo Lima Nicolau <dlimanic@cern.ch> - 1.4.0-1
- [MONIT-2384] Refactor connection to YUM database

* Mon Nov 11 2019 Gonzalo Menéndez Borge <gmenende@cern.ch> - 1.3.0-1
- [MONIT-2289] Add new unfinished-transactions metric

* Tue Oct 01 2019 Simone Brundu <simone.brundu@cern.ch> - 1.2.0-1
- [MONIT-2221] Close connection to yum db after run

* Mon Aug 05 2019 Borja Garrido <borja.garrido.bear@cern.ch> - 1.1.0-1
- [MONIT-2170] Add new metrics to the plugin: transaction_errors, multilib_version_errors

* Thu Jun 20 2019 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.2-1
- Avoid plugin to crash hardly when yum can't be contacted

* Tue Jun 18 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.1-4
- Fix errors in case repo ends in '/' in broken repos.

* Mon Jun 17 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.1-3
- Check metadata xml in broken repos instead of base url.

* Fri Jun 14 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.1-2
- Exported functions to split the 2 dispatches.

* Wed Jun 05 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.1-1
- Added repos broken metric.

* Tue Jun 04 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.0-1
- Initial package.
