import sys
import os
import pytest
from mock import MagicMock, Mock, PropertyMock, patch

NOTIF_FAILURE = 4
ACTUATORS_DIR = './data'

@pytest.fixture
def collectd_yum():
	collectd = MagicMock()
	with patch.dict('sys.modules', {'collectd': collectd}):
		import collectd_yum
		yield collectd_yum

def test_plugin_registration(collectd_yum):
	collectd_yum.collectd.register_config_assert_called_once_with(collectd_yum.configure_callback)

def test_configure_should_not_fail(collectd_yum):
	collectd_yum.configure_callback(Mock(children = []))