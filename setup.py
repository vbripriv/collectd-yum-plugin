from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
NEWS = open(os.path.join(here, 'NEWS.txt')).read()

version = '1.4.0'

install_requires = [
    'pyyaml'
]


setup(name='collectd_yum',
    version=version,
    description="Collectd Plugin retrieve the status of yum",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd yum error python plugin',
    url = 'https://gitlab.cern.ch/monitoring/collectd-yum-plugin',
    author='Simone Brundu',
    author_email='simone.brundu@cern.ch',
    maintainer='CERN IT Monitoring',
    maintainer_email='monit-support@cern.ch',
    license='Apache II',
    packages=find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)
